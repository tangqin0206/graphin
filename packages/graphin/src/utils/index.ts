import hexToRgba from './hexToRgba';
import mock from './mock';
import debug from './debug';

export default { hexToRgba, debug, mock };
